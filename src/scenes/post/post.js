import React from "react";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  List,
  StyleSheet
} from "react-native";
import Card from "../../components/card/card";
import Base from "./postBase";
import Icon from "react-native-vector-icons/Entypo";
import { Header } from "../../components/header/header";
import ActionButton from "react-native-action-button";
import Ionicons from "react-native-vector-icons/Ionicons";
import { Loading } from "../../components/loading";
export default class Post extends Base {
  render() {
    // const {navigation} = this.props;
    console.log("the props of post scene", this.props);

    console.log("this is props of post", this.props.screenProps);
    if (this.state.isLoading) {
      return <Loading />;
    } else {
      // {!this.state.loading &&
      return (
        // <ScrollView style={{ marginBottom: 5 }}>
        <View style={{ marginBottom: 5 }}>
          <FlatList
            data={this.state.postList}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => {
              return (
                <Card
                  postProp={item}
                  likeCount={this.state.likesCount}
                  addLike={() => {
                    this.addLike(item._id);
                  }}
                  // onPress={() => {
                  //   console.log("this is singlepost press ");
                  //   const { mainNavigate } = this.props.screenProps;
                  //   // mainNavigate("SinglePost");
                  // const { navigation } = this.props;
                  // console.log("this is navigation of post ", { navigation });
                  // navigation.navigate("SinglePost");
                  // }}
                />
              );
            }}
            onEndReachedThreshold={0.1}
            onEndReached={({ distanceFromEnd }) => {
              this.scrollPaging();
            }}
          />

          <ActionButton
            buttonColor="rgba(231,76,60,1)"
            size={35}
            position="right"
          >
            <ActionButton.Item
              buttonColor="#9b59b6"
              title="Most Liked"
              onPress={() => {
                console.log("notes tapped!");
                this.getPosts({}, { likes: -1 }, 0);
              }}
            >
              <Ionicons name="md-create" style={styles.actionButtonIcon} />
            </ActionButton.Item>
            <ActionButton.Item
              buttonColor="#3498db"
              title="Most Commented"
              onPress={() => {
                this.getPosts({}, { commentCount: -1 }, 0);
              }}
            >
              <Ionicons
                name="md-notifications-off"
                style={styles.actionButtonIcon}
              />
            </ActionButton.Item>
            <ActionButton.Item
              buttonColor="#1abc9c"
              title="Oldest First"
              onPress={() => {
                this.getPosts({}, { date: 1 }, 0);
              }}
            >
              <Ionicons name="md-done-all" style={styles.actionButtonIcon} />
            </ActionButton.Item>
            <ActionButton.Item
              buttonColor="#1abc9c"
              title="Latest First"
              onPress={() => {
                this.getPosts({}, { date: -1 }, 0);
              }}
            >
              <Ionicons name="md-done-all" style={styles.actionButtonIcon} />
            </ActionButton.Item>
          </ActionButton>
        </View>
        // </View>
        // </ScrollView>
      );
    }
  }
}
const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "black"
  }
});
