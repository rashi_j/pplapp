import React from "react";
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  Alert,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import { InputField } from "../../components/inputField";
import { SubmitButton } from "../../components/button";
import Base from "./loginBase";
export default class Login extends Base {
  render() {
    const { navigation } = this.props;
    console.log("the navigation in render",navigation);
    return (
      <KeyboardAvoidingView keyboardShouldPersistTaps={"always"}>
        <ScrollView keyboardShouldPersistTaps={"always"}>
          <View style={styles.parent}>
            <View style={styles.header}>
              {/* <Image source={{uri:''}} /> */}
              <Text style={styles.title}>Login</Text>
            </View>
            <View style={styles.login}>
              <InputField
                style={{ marginTop: 50 }}
                placeholder="Email"
                value={this.state.email}
                onChangeText={e => {
                  this.handleChange("email", e);
                }}
              />
              <InputField
                placeholder="Password"
                value={this.state.password}
                onChangeText={e => {
                  this.handleChange("password", e);
                }}
                secureTextEntry={true}
              />
              <Text> {this.state.wrongUser || ""}</Text>
              <SubmitButton
                value=" Login "
                onPress={e => {
                  this.loginFunc(e);
                }}
              />
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("SignUp");
                }}
              >
                <Text>Create a new Account</Text>
              </TouchableOpacity>
             
               {/* <SubmitButton
                value="Payments "
                onPress={()=>{this.customer();}}
                // }}
                
              />
              <SubmitButton
                value="advisor "
                onPress={()=>{navigation.navigate("Card")}}
                // }}
                
                             />
              <SubmitButton
                value="list"
                onPress={()=>{this.listCard();}}
                // }}
                
              /> */}
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  parent: {
    flex: 1
  },
  header: {
    height: 100,
    backgroundColor: "#3D3B3B"
  },
  login: {
    flex: 2,
    flexDirection: "column",
    alignItems: "center"
  },
  title: {
    marginTop: 20,
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
    color: "white"
  },
  registerLine: {
    marginTop: 50,
    backgroundColor: "orange"
  }
});
