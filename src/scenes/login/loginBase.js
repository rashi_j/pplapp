import React from "react";
import { Alert, Linking, Platform, Text } from "react-native";
import * as api from "../../api/api";
import * as user from "../../utilities/storage";
export default class SignUpBase extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      wrongUser: "",
      URL: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.loginFunc = this.loginFunc.bind(this);
    this.customer = this.customer.bind(this);
    this.listCard = this.listCard.bind(this);
  }

  handleChange(key, e) {
    this.setState({ [key]: e });
  }


  loginFunc(e) {
    const { navigation } = this.props;
    if (this.state.email === "") {
      Alert.alert("Email is mandatory!!");
    } else if (this.state.password === "") {
      Alert.alert("Password is mandatory!!");
    } else {
      api
        .post("/login", {
          email: this.state.email,
          password: this.state.password,
          status: "active"
        })
        .then(data => {
          console.log("this is the login data", data);
          if (data.error) {
            this.setState({ wrongUser: data.error });
          } else {
            user.set("user", data);
            navigation.navigate("Home");
          }
        })
        .catch(() => {});
    }
  }
  customer()
  { 
    console.log("the function us working");
    fetch('https://api.stripe.com/v1/tokens?card[number]=4242424242424242&card[exp_month]=1&card[exp_year]=2030&card[cvc]=333&amount=999&currency=usd', {
  method: 'POST',
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": "Bearer sk_test_mPpjf9O07bZc18qdv3BnzLMC"
  }
})
  .then(resp => resp.json())
    .then(data => {
      // HERE WE HAVE ACCESS TO THE TOKEN TO SEND IT TO OUR SERVERS
      // ALONG WITH INSENSITIVE DATA      
      console.log("the data of the toeken is",data);
      api.post("/customer", {stripeToken: data.id,cardId:data.card.id})
  
    .then(resp => resp.json())
      .then(function(response) {console.log("the response us",response); })
})
  }
  listCard()
  {   console.log("in the list function of front end");
     api.get("/list")
    .then((response)=> {console.log("the response us",response); })

  }
}
