import React from "react";
import { View } from "react-native";
import Card from "../../components/card/card";
import Base from "./singlePostBase";
import { Loading } from "../../components/loading";

export default class SinglePost extends Base {
  render() {
    const { item } = this.state;
    console.log("this is state of single post ", this.state);
    if (this.state) {
      return <Loading />;
    } else {
      return <Card postProp={item} />;
    }
  }
}
