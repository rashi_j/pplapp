import React from "react";
import { View } from "react-native";
import Card from "../../components/card/card";
import * as api from "../../api/api";
import * as user from "../../utilities/storage";
export default class SinglePostBase extends React.component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      category: "",
      image: "",
      date: "",
      likes: [],
      commentText: "",
      comment: [],
      firstname: "",
      lastname: "",
      username: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.addComment = this.addComment.bind(this);
  }

  async componentWillMount() {
    // }
    try {
      let userInfo = await user.get("user");
      // this.setState({ firstname: userInfo.firstname });
      this.setState({ username: userInfo.email });
    } catch (err) {
      console.log("there is some error");
    }
    console.log("this is state before", this.state);
    let id = this.props.match.params.id;
    console.log("iddddddd", id);
    api.get(`/post/singlePost/${id}`).then(data => {
      console.log("data", data);
      console.log("after state", this.state);
      this.setState({ ...data });
    });
  }
  catch(err) {
    console.log(err);
  }

  handleChange(key, e) {
    this.setState({ [key]: e.target.value });
  }

  addComment(e) {
    e.preventDefault();
    console.log("in the add comment function");
    // let user = localStorage.getItem("user")
    // let userJson = JSON.parse(user);
    // console.log("state -->>", this.state);
    // this.state.username = userJson.email;
    //const { title, category, image } = this.state;
    console.log("state us as", this.state);
    let post_id = this.props.match.params.id;
    let data = {
      id: post_id,
      firstname: userJson.firstname,
      comment: this.state.commentText,
      username: this.state.username
    };

    api.post("/post/singlepost/comment", data).then(() => {
      console.log("commment from");
      this.state.comment.push({
        username: this.state.username,
        comment: this.state.commentText,
        firstname: userJson.firstname
      });
      // this.state.commentText="";
      // this.setState({commentText:""});
      this.setState({ commentText: "" });
    });
  }
}
