import React from 'react';
import { View ,Text} from 'react-native';
import Base from './cardFormBase';
import { InputField } from '../../components/inputField';
import { SubmitButton } from '../../components/button';

export default class CardForm extends Base{
  render() {
    return (
   <View style={{felx:1}}>
      <InputField   placeholder="Title" onChangeText={(e) => { this.handleChange("message", e) }} />
      <SubmitButton value="send" onPress={(e) => { this.send(); }} />
      { this.state.messageList &&
        this.state.messageList.map((data)=>{
          return(
            <View>
              <Text style={{color:"green"}}>{data}</Text>
              </View>
          )
        })
      }
     </View>
 );
  }
}

 
// Regardless of what navigator you use (I like react-native-router-flux) when you 