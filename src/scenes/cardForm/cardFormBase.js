import React from 'react';
window.navigator.userAgent = 'react-native';
import io from 'socket.io-client/dist/socket.io';
const socket=undefined;
export default class CardFormBase extends React.Component{
  
  constructor(props)
  {
    super(props);
    this.state={
      message:"",
      messageList:[],
    }
 socket = io('http://192.168.100.44:3000');
    socket.on("message",((data)=>{console.log("the message is received from server",data.message);
    let message = this.state.messageList.concat(data.message);
    this.setState({messageList:message});
  }))
    this.handleChange = this.handleChange.bind(this);
  }
  send=()=>{
    let message = this.state.messageList.concat(this.state.message);
    this.setState({messageList:message});
   
    socket.emit("message",{"data":this.state.message});
  
  
  }
//   componentWillMount()
//   { 
//   socket.on('connect',(message)=>{console.log("socket is on ");
// })
//     socket.on("message",((data)=>{console.log("the message is received from server",data); }))
//   }

  handleChange(key, e) {
    const state = this.state;
    state[key] = e;
    this.setState(state);
}
}