import React from 'react';
import { Image ,
Alert} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import * as user from '../../utilities/storage';
import * as api from '../../api/api';
export default class CategoryFormBase extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            category: "",
            image: "",
            username: "",
            categoryList: [],
            ImageSource:null,
            modalVisible:false,
        }
        //  this.handleChange = this.handleChange.bind(this);
        //  this.addCategory = this.addCategory.bind(this);

    }
    async componentWillMount() {

        try {
            let userInfo = await user.get("user");
            this.setState({ firstname: userInfo.email });
        } catch (err) {
            console.log("there is some error");
        }
        let data = await api.get('/category/list');
       
        this.state.categoryList = data;
        this.setState({});
       }
   
       selectPhotoTapped = () => {
        //    console.log("the tapped is working");s
        // const options = {
        //     quality: 1.0,
        //     maxWidth: 500,
        //     maxHeight: 500,
        //     storageOptions: {
        //         skipBackup: true
        //     }
        // }
        ImagePicker.showImagePicker(response => {
            console.log("in the image picker",response);
            let source ={uri:response.uri};
            this.setState({
              ImageSource: response
            });
          })
      }
    addCategory = () => {
        let formData = new FormData();
        formData.append('category', this.state.category);
        formData.append("image", {type: "image/jpeg", name:this.state.ImageSource.name, uri:this.state.ImageSource.uri,size:this.state.ImageSource.fileSize});
        fetch('http://192.168.100.44:7070/category/addCategory', {
            method: "POST",
            body: formData,
            headers:{
                "content-type":"multipart/form-data",
                Accept:"application/json"
            }
        }).then(response => response.json()).then(result => {
            console.log('result is as', result);
            // this.props.history.push("/home");
            this.state.categoryList.push(result);
            this.setState({});
            const { mainNavigate } = this.props.screenProps;
         
            mainNavigate("Category")
            
            
        })

    }

    handleChange = (key, e) => {
        const state = this.state;
        state[key] = e;
        this.setState(state );


    }
    getCategory()
    {
        api.get("/category/list").
        then((data) => {
          this.state.categoryList = data;
          this.setState({});
        }
        )
        .
        catch(() => { })
    }

}