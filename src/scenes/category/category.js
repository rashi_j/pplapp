import React from "react";
import {
  View,
  Image,
  StyleSheet,
  Text,
  ScrollView,
  FlatList,
  Modal,
  TouchableHighlight,
  TouchableOpacity
} from "react-native";
import { SubmitButton } from "../../components/button";
import Base from "./categoryBase";
import { ImagePicker } from "react-native-image-picker";
import { InputField } from "../../components/inputField";
import Icon from "react-native-vector-icons/Entypo";
export default class Category extends Base {
  render() {
    return (
      <View style={styles.parent}>
        <Modal
          animationType="fade"
          transparent={false}
          // presentationStyle="formSheet"
          visible={this.state.modalVisible}
          onRequestClose={() => {
            const { mainNavigate } = this.props.screenProps;
            console.log("the mainNavigation", mainNavigate);
            mainNavigate("Category");
            this.setState({ modalVisible: false });
            // alert('Modal has been closed.');
          }}
        >
          <View style={{ backgroundColor: "rgba(255,255,255,1)" }}>
            <ScrollView>
              <View style={styles.modalParent}>
                <View
                  style={{
                    width: 400,
                    height: 40,
                    backgroundColor: "#3D3B3B"
                  }}
                >
                  <Text
                    style={{ color: "white", fontSize: 20, marginLeft: 20 }}
                  >
                    Category
                  </Text>
                </View>

                <InputField
                  style={{
                    width: 300,
                    height: 40,
                    marginTop: 10,
                    borderColor: "#3D3B3B"
                  }}
                  placeholder="Category "
                  onChangeText={e => {
                    this.handleChange("category", e);
                  }}
                />

                {/* <TypeInput onChangeText={(e) => { this.handleChange("title", e) }} /> */}

                <TouchableOpacity
                  onPress={() => {
                    this.selectPhotoTapped();
                  }}
                >
                  <View style={styles.ImageContainer}>
                    {this.state.ImageSource === null ? (
                      <Text>Select a Photo</Text>
                    ) : (
                      <Image
                        style={styles.ImageContainer}
                        source={{ uri: this.state.ImageSource.uri }}
                      />
                    )}
                  </View>
                </TouchableOpacity>

                <SubmitButton
                  value="Upload Category"
                  onPress={() => {
                    this.addCategory();
                    this.setState({ modalVisible: false });
                  }}
                  style={{ width: 400, height: 40, backgroundColor: "#3D3B3B" }}
                />

                <SubmitButton
                  value="Cancel"
                  onPress={e => {
                    // this.addCategory(e);
                    const { mainNavigate } = this.props.screenProps;

                    mainNavigate("Category");
                    this.setState({ modalVisible: false });
                  }}
                  style={{ width: 400, height: 40, backgroundColor: "#3D3B3B" }}
                />
              </View>
            </ScrollView>
          </View>
        </Modal>
        <View style={styles.title}>
          <Text style={styles.titleText}>Category List</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={[styles.commonView, styles.list]}>
            {this.state.categoryList &&
              this.state.categoryList.map(item => {
                console.log("the value of categoory list is", item);
                console.log("the image info is", item.image);
                return (
                  <View style={[styles.commonView, styles.card]}>
                    <View style={[styles.commonView, styles.pic]}>
                      <Image
                        source={{
                          uri: "http://192.168.100.44:7070/static/" + item.image
                        }}
                        style={styles.pic}
                      />
                    </View>
                    <View
                      style={[
                        styles.commonView,
                        styles.nameView,
                        { alignItems: "center" }
                      ]}
                    >
                      <Text
                        style={{
                          color: "black",
                          fontSize: 20,
                          fontWeight: "bold"
                        }}
                      >
                        {item.category}
                      </Text>
                    </View>
                  </View>
                );
              })}
          </View>
        </ScrollView>
        <View style={styles.actionView}>
          <Icon
            name="plus"
            style={styles.icon}
            onPress={
              (setModalVisible = visible => {
                this.setState({ modalVisible: visible });
              })
            }
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  parent: {
    flex: 1,
    flexDirection: "column",
    // backgroundColor: "white",
    marginTop: 10
  },
  title: {
    height: 50,
    backgroundColor: "#3D3B3B",
    alignItems: "center"
  },
  titleText: {
    paddingTop: 12.5,
    fontSize: 25,
    color: "white",
    fontWeight: "bold"
  },
  card: {
    borderColor: "#3D3B3B",
    flexDirection: "row",
    justifyContent: "flex-start",
    marginBottom: 10,
    borderRadius: 10,
    borderWidth: 1
  },
  list: {
    // height: 400,
    // borderColor: "green",
    // marginBottom: 10,
  },
  commonView: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5
    // marginBottom: 5,
  },
  pic: {
    height: 70,
    width: 150,
    backgroundColor: "red"
  },
  nameView: {
    // fontSize: 15,
    // backgroundColor: "pink",
    justifyContent: "flex-end"
    // height: 70,
    // width: 120
  },
  ImageContainer: {
    borderRadius: 10,
    width: 330,
    height: 200,
    marginTop: 10,
    borderColor: "#3D3B3B",
    // borderColor: '#9B9B9B',
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center"
    // backgroundColor: '',
  },
  button: {
    alignItems: "center",
    backgroundColor: "#3D3B3B",
    position: "absolute",
    bottom: 10
  },
  modalParent: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#3D3B3B",
    borderWidth: 1,
    marginTop: 30,
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 3
  },
  icon: {
    fontSize: 25,
    color: "white"
  },
  actionView: {
    height: 35,
    width: 35,
    backgroundColor: "rgba(231,76,60,1)",
    borderColor: "rgba(231,76,60,1)",
    borderWidth: 1,
    borderRadius: 17.5,
    position: "absolute",
    bottom: 10,
    right: 15,
    alignItems: "center",
    elevation: 5,
    justifyContent: "center"
  }
});
