import React from 'react';
import {Alert} from 'react-native';
import * as api from '../../api/api';

export default class SignUpBase extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: "",
            lastname: "",
            email: "",
            password: "",
            username: "",
            error:"",
        }
        this.handleChange = this.handleChange.bind(this);
        this.registerFunc = this.registerFunc.bind(this);
    }
    handleChange(key, e) {
        this.setState({ [key]: e });
    }
    registerFunc(e) {
        const {navigation } = this.props;
         if (this.state.username === '') {
            Alert.alert("please fill the username");
        }
        else if (this.state.firstname === '') {
            Alert.alert("please fill firstname");

        }
        else if (this.state.lastname === '') {
            Alert.alert("please fill the lastname")
        }
        else if (this.state.email === '') {
            Alert.alert("Email is mandatory!!");
        }
        else if (this.state.password.length < 6 || this.state.password === '') {
            Alert.alert("invalid password!!");

        }
        else {
            this.setState({noError:"Please Verify your mail"});
            let data = {
                username: this.state.username,
                email: this.state.email,
                password: this.state.password,
                firstname: this.state.firstname,
                lastname: this.state.lastname
              }
            api.post("/registerUser", data).then((data) => {
               if(data.error)
               {
                   this.setState({error:"user Already exists"});
               }
               else{
                 this.setState({});
                //  navigation.navaigate("Login");
               }
          
              })
        }
    }
}


// 