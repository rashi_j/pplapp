import React from "react";
import { View, Text, AsyncStorage, StyleSheet, ScrollView } from "react-native";
import Base from "./homeBase";
import { SubmitButton } from "../../components/button";
import { Header } from "../../components/header/header";
import Post from "../post/post";
import Category from "../category/category";
import { TabNavigator, TabBarBottom } from "react-navigation";
import FontIcon from "react-native-vector-icons/FontAwesome";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import PostForm from "../postForm/postForm";
import Card from "../../components/card/card";
const TabNav = TabNavigator(
  {
    Post: { screen: Post },
    PostForm: { screen: PostForm, navigationOptions: { header: null } },
    Category: { screen: Category, navigationOptions: { header: null } }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === "Post") {
          // iconName = 'list-ul';
          return <FontIcon name="list-ul" size={25} color={tintColor} />;
        } else if (routeName === "PostForm") {
          return (
            <MaterialIcon name="add-a-photo" size={25} color={tintColor} />
          );
        } else if (routeName === "Category") {
          return <FontIcon name="plus-circle" size={25} color={tintColor} />;
        }
      }
    }),
    tabBarOptions: {
      activeTintColor: "#3D3B3B",
      inactiveTintColor: "#484545",
      style: {
        backgroundColor: "white"
      },
      tabStyle: {
        borderTopWidth: 1,
        borderColor: "#A79E9E"
      },
      showIcon: true,
      showLabel: false
    },
    tabBarPosition: "bottom"
  }
);
export default class Home extends Base {
  render() {
    const { navigation } = this.props;
    return (
      <TabNav
        screenProps={{
          mainNavigate: navigation.navigate
        }}
      />
    );
  }
}
