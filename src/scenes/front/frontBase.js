import React from "react";
import { Platform, Text, Linking, AsyncStorage } from "react-native";
import * as user from "../../utilities/storage";

export default class FrontBase extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    };
  }
  componentWillMount() {
    this.userData();
  }

  userData = async () => {
    const { navigation } = this.props;
    try {
      let userInfo = await user.get("user");
      console.log(userInfo);
      if (userInfo !== null) {
        this.setState({ isLoading: true });
        navigation.navigate("Home");
      }
      // else {
      //   navigation.navigate("Login");
      // }
    } catch (err) {}
  };

  componentDidMount() {
    // B
    // if (Platform.OS === 'android') {
    Linking.getInitialURL().then(url => {
      this.handleOpenURL(url);
    });
    // } else {
    Linking.addEventListener("url", this.handleOpenURL);
  }
  componentWillUnmount() {
    // C
    Linking.removeEventListener("url", this.handleOpenURL);
  }
  handleOpenURL = event => {
    // D
    event.url && this.props.navigation.navigate("Login");
  };
}
