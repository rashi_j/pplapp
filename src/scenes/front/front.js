import React from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  ScrollView,
  AsyncStorage
} from "react-native";
import * as user from "../../utilities/storage";
import { Loading } from "../../components/loading";
import { SubmitButton } from "../../components/button";
import Base from "./frontBase";
export default class Front extends Base {
  render() {
    const { navigation } = this.props;
    this.props.screenProps && navigation.navigate("Login");
    if (this.state.isLoading) {
      return <Loading />;
    } else {
      return (
        <ScrollView style={styles.frontView}>
          <View>
            <Text style={styles.title}>Welcome To PPL !!</Text>
            <View style={styles.imageView}>
              <Image
                source={require("../../images/images/img_9.png")}
                style={styles.image}
              />
            </View>
            <View style={styles.view}>
              <SubmitButton
                value=" Sign Up "
                onPress={() => {
                  navigation.navigate("SignUp");
                }}
                style={{
                  backgroundColor: "white",
                  color: "black"
                }}
              />
              <SubmitButton
                value=" Login"
                onPress={() => {
                  navigation.navigate("Login");
                }}
                style={{ backgroundColor: "white", color: "black" }}
              />
            </View>
          </View>
        </ScrollView>
      );
    }
  }
}
const styles = StyleSheet.create({
  frontView: {
    backgroundColor: "#d5deed",
    borderWidth: 3,
    borderColor: "white"
  },

  title: {
    color: "white",
    marginTop: 20,
    fontSize: 40,
    textAlign: "center",
    fontWeight: "bold"
  },
  view: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  imageView: {
    marginTop: 50,
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
    // borderColor:"",
  },
  image: {
    height: 300,
    width: 300,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 150
  }
});
