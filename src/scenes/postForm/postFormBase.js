import React from "react";
import { Image } from "react-native";
import ImagePicker from "react-native-image-picker";
import * as user from "../../utilities/storage";
import * as api from "../../api/api";
export default class PostFormBase extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ImageSource: null,
      username: "",
      image: "",
      title: "",
      category: "",
      firstname: "",
      lastname: "",
      date: new Date(),
      time: new Date().toLocaleTimeString("en-US"),
      categoryList: []
    };
  }
  async componentWillMount() {
    try {
      let userInfo = await user.get("user");
      this.setState({ firstname: userInfo.firstname });
      this.setState({ lastname: userInfo.lastname });
      this.setState({ username: userInfo.email });
    } catch (err) {
      console.log("there is some error");
    }
    let data = await api.get("/category/list");

    this.state.categoryList = data;
    this.setState({});
  }

  selectPhotoTapped = () => {
    ImagePicker.showImagePicker(response => {
        this.setState({
          ImageSource: response
        });
      })
  }
  createPost = () => {
    let formData = new FormData();
    formData.append("image", {type: "image/jpeg", name:this.state.ImageSource.name, uri:this.state.ImageSource.uri,size:this.state.ImageSource.size});
    formData.append("title", this.state.title);
    formData.append("category", this.state.category);
    //    formData.append('date',this.state.date);
    formData.append("time", this.state.time);
    formData.append("username", this.state.username);
    formData.append("firstname", this.state.firstname);
    formData.append("lastname", this.state.lastname);
    fetch("http://192.168.100.44:7070/post/createPost", {
      method: "POST",
      body: formData,
      header:{
        'Accept': 'application/json',
      'Content-Type': 'multipart/form-data'
      }
    })
      .then(response => response.json())
      .then(result => {
        console.log("result is as", result);
        //    this.props.history.push("/home");
        const { mainNavigate } = this.props.screenProps;
        mainNavigate("Home");
      });
  };

  handleChange(key, e) {
    const state = this.state;
    // switch (e.target.name) {
    //     case 'image':
    //         state.image = e.target.files[0];
    //         break;
    //     default:
    state[key] = e;
    // }
    this.setState(state);
    console.log("state", state);
  }
}
