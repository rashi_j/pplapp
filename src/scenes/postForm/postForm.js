import React from 'react';
import {
    View, StyleSheet, Text,
    Picker,
    PixelRatio,
    TouchableOpacity, ScrollView,
    Image,TypeInput
} from 'react-native';
import { InputField } from '../../components/inputField';
import ImagePicker from 'react-native-image-picker';
import Base from './postFormBase';
import { SubmitButton } from '../../components/button';
import { Select, Option } from 'react-native-chooser';
export default class PostForm extends Base {
    render() {
        console.log("this state category", this.state.category);
        return (
            <ScrollView >
                <View style={styles.parent}>
                <View style={ styles.commonView}>
                       <Text style={styles.postText}>Post</Text>
                       </View>
                   
                    <InputField  style={[styles.commonStyle,styles.backgoundStyle]} placeholder="Title" onChangeText={(e) => { this.handleChange("title", e) }} />
                   
                    {/* <TypeInput onChangeText={(e) => { this.handleChange("title", e) }} /> */}
                   
                   
                   
                    <View style={[styles.backgroundStyle,styles.pickerView]}>
                    <Picker
                        mode="dropdown" 
                        style={ [styles.commonStyle,styles.backgroundStyle]}
                        onValueChange={(itemValue, itemIndex) => { this.setState({ category: itemValue }) }} selectedValue={this.state.category}>
                        <Picker.Item label="Category" value="Category" />
                        {
                            this.state.categoryList && this.state.categoryList.map((item) => {
                                return <Picker.Item  key={item._id} label={item.category} value={item._id} />
                            }
                            )
                        }


                    </Picker>
                    </View>



                    <TouchableOpacity onPress={() => { this.selectPhotoTapped() }}>
                        <View style={styles.ImageContainer}>
                            {this.state.ImageSource === null ? <Text>Select a Photo</Text> :
                                <Image style={styles.ImageContainer} source={{ uri: this.state.ImageSource }} />
                            }
                        </View>
                    </TouchableOpacity>
                     
                   
                    <SubmitButton value="Upload Post" onPress={(e) => { this.createPost(e) }} style={styles.commonView} />
                
                </View>
            </ScrollView>

        );
    }

}

const styles = StyleSheet.create({
    parent: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        borderColor:"#3D3B3B",
        borderWidth:1,
        marginTop:10,
    marginLeft:5,
    marginRight:5,
    
    borderRadius:3
    },
   
    ImageContainer: {
        borderRadius: 10,
         width: 330,
        height: 200,
        marginTop: 10,
        borderColor:"#3D3B3B",
        // borderColor: '#9B9B9B',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '',
    },
    postText:{color:"white",fontSize:20,marginLeft:20

    },
    commonView:{
        width:400,height:40,backgroundColor:"#3D3B3B",
        
    },
  commonStyle:{ width: 300, height: 40, marginTop: 10,

  }  ,
  backgroundStyle:{
      
    borderWidth: 1 ,borderColor:"#3D3B3B",marginTop:10, borderRadius:10
  },
  pickerView:{
      alignItems:"center",
  }

})