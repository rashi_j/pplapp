import React from "react";
import { TextInput, View, StyleSheet, ActivityIndicator } from "react-native";
export const Loading = props => {
  let { style } = props;
  return (
    <View
      style={{
        flex: 1,
        // backgroundColor: "orange",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <ActivityIndicator size="large" color="black" />
    </View>
  );
};
