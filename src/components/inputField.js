import React from 'react';
import {TextInput,View, StyleSheet} from 'react-native';
export const InputField =(props)=>
{ let {style}=props;
    return (
        <TextInput  {...props} style={[LOCAL_STYLES.InputField, style]} underlineColorAndroid={"transparent"} />
    );

}

const LOCAL_STYLES = StyleSheet.create({
    InputField : {
        marginTop:10,
        alignItems:"center",
        borderColor:"gray",
        borderWidth:1,
        color:"black",
        fontSize:15,
        height:40,
        width:200,
        borderRadius:10    }
})