import React from "react";
import { View, TouchableHighlight, Text, StyleSheet } from "react-native";
export const SubmitButton = props => {
  let { value, onPress, style } = props;
  return (
    <TouchableHighlight
      style={[LOCAL_STYLE.button, style]}
      onPress={onPress}
      underlayColor="yellow"
    >
      <View style={[LOCAL_STYLE.viewStyle, style]}>
        <Text style={[LOCAL_STYLE.textStyle, style]}>{value}</Text>
      </View>
    </TouchableHighlight>
  );
};

const LOCAL_STYLE = StyleSheet.create({
  textStyle: {
    color: "white",
    textAlign: "center",
    padding: 5,
    fontSize: 20
  },
  viewStyle: {
    backgroundColor: "#3D3B3B",
    height: 40,
    width: 100
  },
  button: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    width: 100
  }
});
