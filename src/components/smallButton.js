import React from 'react';
import { View, TouchableHighlight, Text, StyleSheet } from 'react-native';
export const SmallButton = (props) => {
    let { value, onPress, style } = props;
    return (
        <TouchableHighlight style={[LOCAL_STYLE.button,style]} onPress={onPress} underlayColor="yellow">
            <View style={[LOCAL_STYLE.viewStyle,style ]}>
                <Text style={LOCAL_STYLE.textStyle}>{value}</Text>
            </View>
        </TouchableHighlight>

    );
}

const LOCAL_STYLE = StyleSheet.create({
    textStyle: {
        color: "orange",
        textAlign: "center",
        padding: 2,
        fontSize: 15

    },
    viewStyle: {

        backgroundColor: "#f0f0f0",
        height:30,
        width: 50
    },
    button: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginTop:20,
        width:50 ,
        marginRight:5,
        marginLeft:5,
        fontWeight:"bold"


    }

})