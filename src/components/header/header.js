import React from "react";
import {
  View,
  TouchableHighlight,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from "react-native";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
export const Header = props => {
  console.log("the props of header is", props);
  return (
    <View
      style={{
        // backgroundColor: "orange",
        flexDirection: "row",
        justifyContent: "space-between"
      }}
    >
      <TouchableOpacity onPress={() => {}}>
        <Icon name="paw" size={30} color="black" />
      </TouchableOpacity>

      <TouchableOpacity
        style={{ marginRight: 10 }}
        onPress={() => {
          // console.log("this is checking",navigation);
          Alert.alert("Log Out", "Are You sure? You Want to Logout", [
            {
              text: "confirm",
              onPress: () => {
                // console.log("thnaks",this.props);
                AsyncStorage.removeItem("user");
                // this.props.nav("Login")
                props.navigation.navigate("Login");
                // clearFunc();
              }
            }
          ]);
        }}
      >
        <Icon name="logout" size={30} color="black" />
      </TouchableOpacity>
    </View>
  );
};

const LOCAL_STYLE = StyleSheet.create({});
