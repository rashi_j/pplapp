import React from "react";
import {
  View,
  TouchableHighlight,
  Text,
  StyleSheet,
  Image
} from "react-native";
import MaterialIcon from "react-native-vector-icons/MaterialCommunityIcons";
import FontIcon from "react-native-vector-icons/FontAwesome";
import UserInfo from "../userInfo";
import MetaInfo from "../metaInfo";
export default (Card = props => {
  let image = props.postProp.image;
  let { onPress, likeCount } = props;
  console.log("the value of props is", props);
  console.log("the count of like", props.postProp.likes.length);
  return (
    <View style={styles.parent}>
      {/* //     <View style={{ flexDirection: "row", justifyContent: "flex-end", marginRight:10}}>
        //         <Text style={styles.text}>{props.postProp.category.category}</Text>
        //     </View> */}
      <View style={styles.header}>
        <UserInfo />
        <MetaInfo
          category={props.postProp.category.category}
          date={props.postProp.date}
        />
      </View>
      <View>
        <Image
          source={{
            uri: "http://192.168.100.44:7070/static/" + props.postProp.image
          }}
          style={styles.image}
        />
      </View>
      <View style={styles.title}>
        <Text style={styles.titleText}>{`"${props.postProp.title}"`}</Text>
      </View>
      <View style={styles.info}>
        {/* <View style={styles.like}> */}
        {props.likeCount === 0 ? (
          <View>
            <FontIcon
              name="heart-o"
              style={styles.icon}
              onPress={props.addLike}
            />
            <Text style={{ fontSize: 10 }}>2</Text>
          </View>
        ) : (
          <View>
            <FontIcon
              name="heart"
              style={styles.icon}
              color="red"
              onPress={() => {
                props.addLike(props.postProp._id);
              }}
            />
            <Text style={{ fontSize: 10 }}>2</Text>
          </View>
        )}

        {/* </View> */}
        {/* <View style={styles.comment}> */}
        <FontIcon name="comment-o" style={styles.icon} onPress={onPress} />
        <Text style={{ fontSize: 10 }}>102</Text>
        {/* </View> */}
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  parent: {
    marginBottom: 24,
    marginTop: 5,
    backgroundColor: "#e8ebef"
  },
  image: {
    height: 200,
    backgroundColor: "white"
  },
  icon: {
    fontSize: 25,
    color: "black",
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10
  },
  info: {
    marginTop: 5,
    flexDirection: "row",
    justifyContent: "flex-end",
    marginBottom: 5,
    marginLeft: 15,
    marginRight: 15
  },
  text: {
    fontFamily: "Comic Sans",
    color: "black",
    fontSize: 15,
    fontWeight: "bold"
  },
  userInfo: {
    height: 80
  },
  header: {
    marginLeft: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 5,
    backgroundColor: "#e8ebef"
  },
  title: {
    height: 25,

    backgroundColor: "#e8ebef"
  },
  titleText: {
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 15,
    backgroundColor: "#e8ebef",

    color: "black",
    fontSize: 15,
    // fontWeight:"bold",
    fontFamily: "Comic Sans",
    fontStyle: "italic"
  }
});
