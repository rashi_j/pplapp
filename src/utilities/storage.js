   import {AsyncStorage}from 'react-native';
   
   get= async (key) => {
        return new Promise(async (resolve, reject)=>{
            let res = await AsyncStorage.getItem(key);
            res= JSON.parse(res);
            return resolve(res);
        })
    }
   set = (key, object) => {
        return new Promise(function (resolve, reject) {
      console.log("user in set user function")
            AsyncStorage.setItem(key, JSON.stringify(object));
        })
    }
export {get,set};