import React, { Component } from "react";
import {
  View,
  Linking,
  Platform,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from "react-native";
import FCM, { FCMEvent } from "react-native-fcm";
import { StackNavigator } from "react-navigation";
import SignUp from "./src/scenes/signUp/signup";
import Login from "./src/scenes/login/login";
import Front from "./src/scenes/front/front";
import Home from "./src/scenes/home/home";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Category from "./src/scenes/category/category";
import { Header } from "./src/components/header/header";
import Card from "./src/scenes/card/card";
import CardForm from "./src/scenes/cardForm/cardForm";

const StackNav = new StackNavigator(
  {
    SignUp: {
      navigationOptions: {
        header: null
      },
      screen: SignUp
    },
    Login: {
      navigationOptions: {
        header: null
      },
      screen: Login
    },

    Front: {
      navigationOptions: {
        header: null
      },
      screen: Front
    },
    Card: {
      navigationOptions: {
        header: null
      },
      screen: Card
    },
    CardForm: {
      navigationOptions: {
        header: null
      },
      screen: CardForm
    },

    Home: {
      title: "Home",
      navigationOptions: ({ navigation }) => {
        return {
          headerTransparent: true,
          header: <Header navigation={navigation} />
        };
      },
      screen: Home
    }
   
  },
  {
    initialRouteName: "Front"
  }
);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fcm_token: ""
    };
    // this.sendRemote = this.sendRemote.bind(this);
  }
  componentDidMount() {
    
    
    FCM.requestPermissions();
    FCM.getFCMToken().then(token => {
      console.log("TOKEN (getFCMToken)", token);
    });

    // FCM.on(FCMEvent.RefreshToken, token => {
    //   // fcm token may not be available on first load, catch it here
    //   console.log("fcm token in refresh token is as", token);
    // });
    FCM.getInitialNotification().then(notif => {
      console.warn("inside initial notification", notif);
      // this.navigateByNotification(notif);
    });
    this.notificationListener = FCM.on(FCMEvent.Notification, async notif => {
      console.warn("inside fcm.on notif is as", notif);
      let { payload = "{}" } = notif;
      let parsedPayload = JSON.parse(payload);
      console.log("the payload is", parsedPayload);
      Alert.alert("notif");
    });
  }
  // if (notif && notif.opened_from_tray) {
  //   this.navigateByNotification(notif);
  // } else {
  //   let title =
  //     Platform.OS === "ios" ? notif.aps.alert.body : notif.fcm.body;
  //   showSnackbar(title, "OK", () => this.navigateByNotification(notif));
  // }

  // the notification on mobile screen.
  // sendRemote(notif) {
  //   console.log('send');
  //   FCM.presentLocalNotification({
  //     title: notif.title,
  //     body: notif.body,
  //     priority: "high",
  //     click_action: notif.click_action,
  //     show_in_foreground: true,
  //     local: true
  //   });
  // }

  //  clearFunc = () =>{
  //    console.log("removing user from storage");
  //    AsyncStorage.removeItem("user");

  //      // This method display the notification on mobile screen.
  // sendRemote(notif) {
  //   console.log('send');
  //   FCM.presentLocalNotification({
  //     title: notif.title,
  //     body: notif.body,
  //     priority: "high",
  //     click_action: notif.click_action,
  //     show_in_foreground: true,
  //     local: true
  //   })
  // }
  // let  socketConfig = { path: '/socket' };
  // let socket = new SocketIO('localhost:3000', socketConfig);
  // Connect!
  //

  // An event to be fired on connection to socket

  //update your fcm token on server.

  render() {
    return <StackNav />;
  }
}
